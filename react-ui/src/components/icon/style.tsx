import  styled  from 'styled-components'
import {IconProps} from './IconProps'
export const Svg = styled.svg<IconProps>`
    display:${(props)=> (props.block? 'block':'inlineblock')};
    vertical-align:middle;
    shape-rendering:inherit;
    transform: translated3d(0,0,0);
`

export const Path = styled.path`
    fill:${(props)=> props.color}
`
