import React from 'react'
import { Svg,Path } from './style';
import {IconProps} from './IconProps'
import {icons} from '../shared/icons'
export function Icon(props:IconProps) {
    const {block, icon,color} = props 
    return(
        <Svg
         viewBox = "0 0 1024 1024"
         width="20px"
         height="20px"
         block={block}
        {...props}
        >
            <Path d={icons[icon]} color={color}/>
        </Svg>

    )
}

Icon.defaultProps = {
    block: false,
    color: "black"
}
