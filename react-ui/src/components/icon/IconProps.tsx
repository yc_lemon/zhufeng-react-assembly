import {icons} from '../shared/icons'
export interface IconProps {
    /** 图标名*/
	icon: keyof typeof icons;
	/** 是否块级元素 */
	block?: boolean;
	/** 颜色 */
	color?: string;
}