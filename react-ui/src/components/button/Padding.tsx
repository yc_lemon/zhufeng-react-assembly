export type PaddingTypes = keyof typeof PADDINGS;
type paddingType = "small" | "medium";
type paddingObj = {
	[key in paddingType]: paddingType;
};
export const PADDINGS: paddingObj = {
	small: "small",
	medium: "medium",
};