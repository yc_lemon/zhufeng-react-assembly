import React, {  PropsWithChildren, useMemo } from "react";

import { SIZES } from "./Size";
import { APPEARANCES } from "./Appearence";
import {ButtonProps}  from './ButtonProps'
import {Loading,StyledButton,Text} from './style'

// interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> { }










function Button(props: PropsWithChildren<ButtonProps>) {
	const { children, isLoading, loadingText, isLink, } = props;
	const buttonInner = (
		<>
			<Text>{children}</Text>
			{isLoading && <Loading>{loadingText || "Loading..."}</Loading>}
		</>
	)
	const btnType = useMemo(() => {
		if (isLink) {
			return 'a'
		}
	}, [isLink])
	return (
		<StyledButton as={btnType} {...props}>{buttonInner}</StyledButton>
	)
}

Button.defaultProps = {
	isLoading: false,
	loadingText: null,
	isLink: false,
	appearance: APPEARANCES.tertiary,
	isDisabled: false,
	isUnclickable: false,
	containsIcon:false,
	size: SIZES.medium,
	ButtonWrapper: undefined,
}
export default Button;