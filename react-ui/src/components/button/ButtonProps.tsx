import { SizesTypes } from "./Size";
import { AppearancesTypes } from "./Appearence";

import { ReactNode, AnchorHTMLAttributes } from 'react'
import { ButtonHTMLAttributes } from 'react';
export interface CustomButtonProps {
    disabled: boolean;
    isLoading: boolean;
    isLink: boolean;
    loadingText?: ReactNode;
    size?: SizesTypes;
    /** 按钮类型 */
    appearance?: AppearancesTypes;
    /** 无效点击 */
    isUnclickable: boolean,
}


export type ButtonProps = CustomButtonProps & AnchorHTMLAttributes<HTMLAnchorElement> & ButtonHTMLAttributes<HTMLButtonElement>