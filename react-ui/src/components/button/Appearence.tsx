export type AppearancesTypes = keyof typeof APPEARANCES;

type btnType =
	| "primary"
	| "primaryOutline"
	| "secondary"
	| "secondaryOutline"
	| "tertiary"
	| "outline"
	| "inversePrimary"
	| "inverseSecondary"
	| "inverseOutline"
	|"disabled"

type AppearancesObj = {
	[key in btnType]:btnType
}

export const APPEARANCES:AppearancesObj = {
	primary:"primary",
	primaryOutline:"primaryOutline",
	secondary:"secondary",
	secondaryOutline:"secondaryOutline",
	tertiary:"tertiary",
	outline:"outline",
	inversePrimary:"inversePrimary",
	inverseSecondary:"inverseSecondary",
	inverseOutline:"inverseOutline",
	disabled: "disabled"
}